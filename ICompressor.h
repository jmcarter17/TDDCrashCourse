#ifndef ICOMPRESSOR_H
#define ICOMPRESSOR_H


namespace crashCourse
{

class ICompressor
{
public:
    virtual ~ICompressor() = default;

    virtual bool isRunning() = 0;
};


} // namespace crashCourse

#endif // ICOMPRESSOR_H
