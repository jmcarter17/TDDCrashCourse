#ifndef IDIGITALINPUT_H
#define IDIGITALINPUT_H


namespace crashCourse
{

class IDigitalInput
{
public:
    virtual ~IDigitalInput() = default;

    virtual bool getInput() = 0;
};

} // namespace crashCourse

#endif // IDIGITALINPUT_H
