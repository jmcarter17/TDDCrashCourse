#ifndef IRELAY_H
#define IRELAY_H


namespace crashCourse
{

class IRelay
{
public:
    virtual ~IRelay() = default;

    virtual void setRelayOn() = 0;
    virtual void setRelayOff() = 0;
    virtual bool getRelayState() = 0;
};

} // namespace crashCourse

#endif // IRELAY_H
