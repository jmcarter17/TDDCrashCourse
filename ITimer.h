#ifndef ITIMER_H
#define ITIMER_H

namespace crashCourse
{

class ITimer
{
public:
    virtual ~ITimer() = default;

    virtual void start() = 0;
    virtual void stop() = 0;
    virtual int elapsedTime() = 0;
    virtual bool isRunning() = 0;
    virtual bool isExpired(int duration) = 0;
};


} // namespace crashCourse

#endif // ITIMER_H
