#include "OilManager.h"
#include "ICompressor.h"
#include "ITimer.h"

using namespace crashCourse;

OilManager::OilManager()
{
}


void OilManager::setCompressor(ICompressor* context)
{
    pComp = context;
}

void OilManager::setOilRelay(IRelay* relay)
{
    pRelay = relay;
}

void OilManager::setOilLevelLowInput(IDigitalInput* input)
{
    pInput = input;
}

void OilManager::setTimer(ITimer *timer)
{
    pTimer = timer;
}

void OilManager::update()
{
    if(pComp)
        running = pComp->isRunning();
}
