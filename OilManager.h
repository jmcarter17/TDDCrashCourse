#pragma once

#include <QString>
#include <QStringList>

namespace crashCourse
{

//Forward declaration
class ICompressor;
class IDigitalInput;
class IRelay;\
class ITimer;

class OilManager
{
public:
    OilManager();
    void setCompressor(ICompressor* compressor);
    void setOilRelay(IRelay* relay);
    void setOilLevelLowInput(IDigitalInput* input);
    void setTimer(ITimer* timer);

    void update();

protected:
    bool running{0};

private:
    ICompressor* pComp{};
    IRelay* pRelay{};
    IDigitalInput* pInput{};
    ITimer* pTimer{};
};



}// namespace crashCourse
