#ifndef COMPRESSORSTUB_H
#define COMPRESSORSTUB_H

#include "ICompressor.h"

namespace {

class CompressorStub : public crashCourse::ICompressor
{
public:
    bool isRunning() override
    {
        return running;
    }

    bool running{false};
};

} //End of unnamed namespace

#endif // COMPRESSORSTUB_H
