#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "OilManager.h"
#include "ICompressor.h"
#include "CompressorStub.h"
#include "timerstub.h"

using namespace crashCourse;

namespace {

class OilManagerTest : public ::testing::Test, public OilManager
{

protected:
    OilManagerTest(){}
    virtual ~OilManagerTest(){}

    OilManager* manager = nullptr;
    CompressorStub compressor;
    TimerStub timer;

    void SetUp() override{
        manager = this;
        manager->setCompressor(&compressor);
        manager->setTimer(&timer);
    }
};

TEST_F(OilManagerTest, Default_RunningIsFalse)
{
    ASSERT_FALSE(running);
}

TEST_F(OilManagerTest, WhenCompressorIsNotRunning_RunningIsFalse)
{
    compressor.running = false;
    manager->update();
    ASSERT_FALSE(running);
}

TEST_F(OilManagerTest, WhenCompressorIsRunning_RunningIsTrue)
{
    compressor.running = true;
    manager->update();
    ASSERT_TRUE(running);
}

TEST_F(OilManagerTest, WhenInputIsFalse_OilLevelLowIsFalse)
{
    //Set InputToTrue
    //update manager
    //ASSERT_TRUE
}



}  // namespace

