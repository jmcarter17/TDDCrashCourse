#ifndef TIMERSTUB
#define TIMERSTUB

#include "ITimer.h"

namespace {

class TimerStub : public crashCourse::ITimer
{
public:

    void start(){
        running = true;
    }

    void stop(){
        running = false;
        time = 0;
    }

    int elapsedTime(){
        return time;
    }

    bool isRunning(){
        return running;
    }

    bool isExpired(int duration){
        return time > duration;
    }

    void setTime(int newTime){
        time = newTime;
    }

private:
    bool running{false};
    int time{0};
};

} // namespace

#endif // TIMERSTUB

