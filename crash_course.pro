DESTDIR = $$OUT_PWD

QMAKE_CFLAGS += -std=gnu99
# C++11 support (officially supported since Qt 5; else force the GCC flag)
QMAKE_CXXFLAGS += -std=c++11

CONFIG += qt thread staticlib static rtti exceptions warn_on

QT       += core \
            network \

CONFIG   += console
CONFIG   -= app_bundle

SOURCES += \
    OilManager.cpp \
    Tests/OilManagerTest.cpp \
    Tests/main.cpp


HEADERS_TO_MOCK += \


HEADERS += $$HEADERS_TO_MOCK \
    OilManager.h \
    ICompressor.h \
    Tests/CompressorStub.h \
    IRelay.h \
    IDigitalInput.h \
    ITimer.h \
    Tests/timerstub.h
            

INCLUDEPATH +=  \


DEPENDPATH += $$INCLUDEPATH

##################################################################
###### Below is for tests and for automatic mock generation ######
##################################################################
# gmock needs pthread
LIBS += -lpthread

# link to the compiled GMock library
LIBS += -L$$(GMOCK_DIR) -lgmock

# the test class needs the headers from GMock and GTest, along with buildpath
INCLUDEPATH += $$(GTEST_DIR)/include \
               $$(GMOCK_DIR)/include \
               $$OUT_PWD
DEPENDPATH += $$OUT_PWD $$_PRO_FILE_PWD_

#Find all used mocks
SOURCE_FILES = $$SOURCES
for(sourcefile, SOURCE_FILES) {
   DISCOVERED_MOCKS += $$system(grep \"$${LITERAL_HASH}include.*Mock.h.\" $${sourcefile}  | sed -e 's/$${LITERAL_HASH}include..//g' | sed -e 's/.h.\$//g')
}
DISCOVERED_MOCKS = $$unique(DISCOVERED_MOCKS)

#clean mock.makefile
system(mockfile $$OUT_PWD)

#generate mock files and mock.makefile
for(mock, DISCOVERED_MOCKS) {
   INTERFACE_TO_MOCK = $$system(MockNameToDeduceClassName=\"$${mock}\" && echo \"\${MockNameToDeduceClassName%Mock}.h\")

   for(header, HEADERS){
      contains(header, $${INTERFACE_TO_MOCK}) {
         headerDir=$$dirname(header)
         isEmpty(headerDir):headerDir=.
         #create the mock for the first time
         system(mmock \"$$_PRO_FILE_PWD_/$${header}\" \"$$OUT_PWD/$${headerDir}\" \"$${mock}.h\")
         #add the mock's build rule to mocks' makefile
         system(mockfile \"$$OUT_PWD\" \"$${mock}.h\" \"$$_PRO_FILE_PWD_/$${header}\" \"$${headerDir}\" )

         break()
      }
   }
}

#promote mock.makefile to a pre-target dependency
PRE_TARGETDEPS += $$OUT_PWD/mock.makefile
##################################################################
###### Above is for tests and for automatic mock generation ######
##################################################################


